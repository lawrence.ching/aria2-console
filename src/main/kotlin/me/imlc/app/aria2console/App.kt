package me.imlc.app.aria2console

import me.imlc.app.aria2console.apis.AriaNgHandler
import org.eclipse.jetty.server.Server
import org.eclipse.jetty.server.handler.ContextHandlerCollection

class App: AppRunnerApp {

    private lateinit var server: Server

    private fun startUp(appName: String = "aria", port: Int = 8080) {
        server = Server(port)

        val ariaNgHandler1 = AriaNgHandler()
        ariaNgHandler1.contextPath = "/$appName"

        val ariaNgHandler2 = AriaNgHandler()
        ariaNgHandler2.contextPath = "/"

        val collection = ContextHandlerCollection()
        collection.handlers = arrayOf(ariaNgHandler1, ariaNgHandler2)

        server.handler = collection

        server.start()
        server.join()
    }

    override fun start(appName: String, port: Int) {
        startUp(appName, port)
    }
}