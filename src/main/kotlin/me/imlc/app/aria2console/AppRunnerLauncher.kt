package me.imlc.app.aria2console

class AppRunnerLauncher {

    fun launch(app: AppRunnerApp) {
        val settings = System.getenv()

        // When run from app-runner, you must use the port set in the environment variable APP_PORT
        val port = Integer.parseInt(settings.getOrDefault("APP_PORT", "8080"))
        // All URLs must be prefixed with the app name, which is got via the APP_NAME env var.
        val appName = settings.getOrDefault("APP_NAME", "aria")

        app.start(appName, port)
    }

}

interface AppRunnerApp {
    fun start(appName: String, port: Int)
}