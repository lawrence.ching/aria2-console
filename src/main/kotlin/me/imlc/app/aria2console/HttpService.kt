package me.imlc.app.aria2console

import com.google.common.util.concurrent.AbstractIdleService

class HttpService(val apiBasePath:String, val port: Int): AbstractIdleService() {

    override fun startUp() {

    }

    override fun shutDown() {

    }

}