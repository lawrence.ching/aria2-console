package me.imlc.app.aria2console

fun main(args: Array<String>) {
    val launcher = AppRunnerLauncher()
    launcher.launch(App())
}