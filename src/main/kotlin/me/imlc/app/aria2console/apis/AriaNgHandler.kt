package me.imlc.app.aria2console.apis

import org.eclipse.jetty.server.handler.ContextHandler
import org.eclipse.jetty.server.handler.ResourceHandler
import org.eclipse.jetty.util.resource.Resource

class AriaNgHandler(private val resourcePath: String): ContextHandler() {

    constructor(): this("AriaNg")

    init {
        this.baseResource = Resource.newResource(this.javaClass.classLoader.getResource(resourcePath))
        this.handler = ResourceHandler()
    }

}