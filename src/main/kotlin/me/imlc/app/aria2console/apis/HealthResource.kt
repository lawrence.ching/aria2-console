package me.imlc.app.aria2console.apis

import org.json.JSONObject
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Path("health")
class HealthResource {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    fun getHealthy(): String {
        val json = JSONObject()
        json.put("status", "running");
        return json.toString(4)
    }

}