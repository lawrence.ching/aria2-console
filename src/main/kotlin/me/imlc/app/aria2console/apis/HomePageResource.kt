package me.imlc.app.aria2console.apis

import me.imlc.app.aria2console.AppConfig
import javax.inject.Inject
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Path("/")
class HomePageResource {

    @Inject
    private lateinit var config: AppConfig

    @GET
    @Produces(MediaType.TEXT_HTML)
    fun indexHtml(): String {

        val HTML =
"""
<html>
    <head>
        <title>Aria2 Console</title>
    <head>
    <body>
        <h1>Aria2 Console</h1>
        <p>Aria2 Port: ${config.aria2Port}$</p>
        <p>Aria2 Status: Unknown</p>
        <p>Aria</p>
        <p></p>
    </body>
</html>
""".trimIndent()

        return HTML
    }
}